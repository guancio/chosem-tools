#!/usr/bin/python

from ccpom import run_test
import ex1
import ex3
import ex_new_1
import ex_new_2
import ex5
import ex7

import jlamp

#run_test(ex1.graphs, "log-ex1", ["a", "b", "c", "d"], False)
#run_test(ex3.graphs, "log-ex3", ["a", "b", "c"], False)
#run_test(ex_new_1.graphs, "log-ex_new_1", ["a", "b"], True)
#run_test(ex_new_2.graphs, "log-ex_new_2", ["a", "b"], True)
#run_test(ex5.graphs, "log-ex5", ["a", "b", "c"], True)
#run_test(ex7.graphs, "log-ex7", ["a", "b", "c"], False)

tests = [
    # (jlamp.example_1, "non_det_1", ["a", "b"], False, "fig:cho:cho:1"), 
    # (jlamp.example_2, "non_det_2", ["a", "b", "c"], False, "fig:cho:cho:5"),
    # (jlamp.example_3, "non_det_wrong_1", ["a", "b", "c"], False, "fig:cho:cho:2a"),
    # (jlamp.example_4, "non_det_wrong_2", ["a", "b", "c"], False, "fig:cho:cho:2"),
    # (jlamp.example_5, "non_det_wrong_3", ["a", "b", "c", "d"], False, "fig:cho:cho:6"),
    # (jlamp.example_6, "non_det_wrong_4", ["a", "b", "c"], False, "fig:example:cc3pom"),
    # (jlamp.example_7, "graph_choice_par", ["a", "b", "c", "d"], False, "fig:example:msg")
    # (jlamp.example_8, "parallel_same_msg", ["a", "b", "c"], False, "fig:ex:2-1"),
    (jlamp.case_study[0], "case_study_0", ["a", "b", "c"], False, "0"),
    (jlamp.case_study[1], "case_study_1", ["a", "b", "c"], False, "1")
    # (jlamp.case_study[2], "case_study_2", ["a", "b", "c"], False, "2")
    # (jlamp.case_study[3], "case_study_3", ["a", "b", "c"], False, "3")
]
 
res= []
for t in tests:
    r =  run_test(t[0], t[1], t[2], t[3])
    print r
    res.append((r, t[4]))
for (r, name) in res:
    print (("    $\\exsem[%s]$ &" % name) + 
           " & ".join(
        ["%d"%(r[k]) for k in [
            "cc2-branches", "cc2-tuples", "cc2-ipc", "cc2-errors", "cc2-time",
            "cc3-branches", "cc3-tuples", "cc3-ipc", "cc3-errors", "cc3-time",
            "mcs", "mcs-time"
        ]]
    ) + "\\\\")
    
    # {'cc3-errors': 93, 'cc2-tuples': 125, 'mcs-time': 4.581928253173828, 'cc2-time': 88.6991024017334, 'cc3-tuples': 1760, 'cc3-time': 1901.0918140411377, 'cc2-ipc': 8, 'cc2-errors': 0, 'cc3-ipc': 102, 'mcs': 16, 'cc2-branches': 15, 'cc3-branches': 0}


#run_test(jlamp.example_1, "non_det_1", ["a", "b"], False)
#run_test(jlamp.example_2, "non_det_2", ["a", "b", "c"], False)
#run_test(jlamp.example_3, "non_det_wrong_1", ["a", "b", "c"], False)
#run_test(jlamp.example_4, "non_det_wrong_2", ["a", "b", "c"], False)
#run_test(jlamp.example_5, "non_det_wrong_3", ["a", "b", "c", "d"], False)
#run_test(jlamp.example_6, "non_det_wrong_4", ["a", "b", "c"], False)
#run_test(jlamp.example_7, "graph_choice_par", ["a", "b", "c", "d"], False)
#run_test(jlamp.example_8, "parallel_same_msg", ["a", "b", "c"], False)
#run_test(jlamp.parallel_same_msg_simple, "parallel_same_msg_simple", ["a", "b"], True)
# run_test(jlamp.case_study[0], "case_study", ["a", "b", "c"], False)
