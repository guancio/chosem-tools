import networkx as nx
from utils import add_pair, debug_graph


def gen_graph():
    gr = nx.DiGraph()
    abx1 = add_pair(gr, "a", "b", 1, "x")
    abx2 = add_pair(gr, "a", "b", 2, "x")
    abx3 = add_pair(gr, "a", "b", 3, "x")
    gr.add_edge(abx1[0], abx2[0])
    gr.add_edge(abx1[0], abx3[0])
    gr.add_edge(abx1[1], abx3[1])
    gr.add_edge(abx2[1], abx3[1])
    return gr


graphs = [gen_graph()]

if __name__ == "__main__":
    print (len(graphs))
    debug_graph(graphs[0])
