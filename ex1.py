import networkx as nx
from utils import add_pair, debug_graph


def gen_graph(a_before_d):
    gr = nx.DiGraph()
    abx = add_pair(gr, "a", "b", 1, "x")
    dby = add_pair(gr, "d", "b", 2, "y")
    acx = add_pair(gr, "a", "c", 3, "x")
    gr.add_edge(abx[0], acx[0])
    dcy = add_pair(gr, "d", "c", 4, "y")
    gr.add_edge(dby[0], dcy[0])
    if a_before_d:
        gr.add_edge(acx[1], dcy[1])
        gr.add_edge(abx[1], dby[1])
    else:
        gr.add_edge(dcy[1], acx[1])
        gr.add_edge(dby[1], abx[1])
    return gr


graphs = [gen_graph(False), gen_graph(True)]

if __name__ == "__main__":
    print (len(graphs))
    debug_graph(graphs[0])
