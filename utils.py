from networkx.drawing.nx_agraph import write_dot
import networkx as nx
import os
import itertools

def debug_graph(gr, name="G"):
    """Some documentation"""
    write_dot(gr, '%s.dot' % name)
    os.system('dot -Tpdf %s.dot -o %s.pdf' % (name, name))


def add_pair(gr, snd, rcv, n, msg):
    n1 = ("%s-%d" % (snd, n))
    n2 = ("%s-%d" % (rcv, n))
    gr.add_node(n1)
    gr.add_node(n2)
    gr.node[n1]['label'] = "%s%s!%s" % (snd.upper(), rcv.upper(), msg)
    gr.node[n2]['label'] = "%s%s?%s" % (snd.upper(), rcv.upper(), msg)
    gr.add_edge(n1, n2)
    return (n1, n2)


def transitive_closure(gr):
    gr1 = nx.transitive_closure(gr)
    for n in gr1.nodes():
        gr1.node[n]["label"] = gr.node[n]["label"]
    return gr1

def transitive_reduction(gr):
    gr = nx.subgraph(gr, gr.nodes())
    for (n1, n2) in gr.edges():
        if len(set(nx.descendants(gr, n1)).intersection(set(nx.ancestors(gr, n2)))) > 0:
            gr.remove_edge(n1,n2)
    return gr

def proj(gr, pr):
    nodes = [x for x in gr.nodes() if x[0] == pr]
    return nx.subgraph(gr, nodes)


def proj_lbl(gr, label):
    nodes = [x for x in gr.nodes() if gr.node[x]["label"] == label]
    return nx.subgraph(gr, nodes)


def map_lbls(gr):
    """ Returns a map label->nodes """
    lbls = {}
    for n in gr.nodes():
        lbl = gr.node[n]["label"]
        if lbl in lbls:
            lbls[lbl].append(n)
        else:
            lbls[lbl] = [n]
    return lbls


def join(pr1, gr1, pr2, gr2):
    gu = nx.union(gr1, gr2)
    lbls1 = map_lbls(gr1)
    lbls2 = map_lbls(gr2)

    gps = []
    perms = {}
    for l1 in lbls1:
        ns1 = lbls1[l1]
        if l1[1] != pr2 and l1[0] != pr2:
            continue
        l2 = "%s%s%s%s" % (l1[0], l1[1], "!" if l1[2] == "?" else "?", l1[3:])
        ns2_perm = itertools.permutations(lbls2[l2])
        perms[l1] = ns2_perm
    gens = [{}]
    for l1 in perms:
        new_gen = []
        for mapping in gens:
            for p in perms[l1]:
                new_map = mapping.copy()
                new_map[l1] = p
                new_gen.append(new_map)
        gens = new_gen
    print gens

    for gen in gens:
        gr = gu.copy()
        for l1 in perms:
            ns1 = lbls1[l1]
            ns2 = gen[l1]
            for i in range(len(ns1)):
                gr.add_edge(ns1[i], ns2[i])
        gr = transitive_closure(gr)
        gps.append(gr)
    return gps


def get_matching_label(lbl):
    return "%s%s%s%s" % (lbl[0], lbl[1],
                         "!" if lbl[2] == "?" else "?", lbl[3:])


def get_all_send_lbls(gr):
    lbls = set()
    for n in gr.nodes():
        lbl = gr.node[n]["label"]
        if lbl[2] == "!":
            lbls.add(lbl)
    return lbls
def get_all_receive_lbls(gr):
    lbls = set()
    for n in gr.nodes():
        lbl = gr.node[n]["label"]
        if lbl[2] == "?":
            lbls.add(lbl)
    return lbls
def get_all_send_nodes(gr):
    nodes = set()
    for n in gr.nodes():
        lbl = gr.node[n]["label"]
        if lbl[2] == "!":
            nodes.add(n)
    return nodes
def get_all_receive_nodes(gr):
    nodes = set()
    for n in gr.nodes():
        lbl = gr.node[n]["label"]
        if lbl[2] == "?":
            nodes.add(lbl)
    return nodes
def get_all_principals(gr):
    return [n[0] for n in gr.nodes()]


def linearizations(gr):
    to_process = [([], gr.nodes())]
    words = []
    while (len(to_process) > 0):
        (s, nodes) = to_process.pop()
        for n in nodes:
            deps = [a for (a, b) in gr.in_edges(n)]
            inter = set(nodes).intersection(set(deps))
            if (len(inter) > 0):
                continue
            nodes1 = [n1 for n1 in nodes]
            nodes1.remove(n)
            s1 = [a for a in s]
            s1.append(n)
            if len(nodes1) == 0:
                words.append(s1)
            else:
                to_process.append((s1, nodes1))
    return words


# several operaions can be optimized. Bu we do not care.
def join_graph(gr, label):
    gps = []
    gr = transitive_closure(gr)
    gr1 = proj_lbl(gr, label)
    gr2 = proj_lbl(gr, get_matching_label(label))

    if label[2] == "?" and len(gr1.nodes()) > len(gr2.nodes()):
        return None
    if label[2] == "!" and len(gr1.nodes()) < len(gr2.nodes()):
        return None

    
    all_lin1 = linearizations(gr1)
    all_lin2 = linearizations(gr2)
    
    for (lin1, lin2) in itertools.product(all_lin1, all_lin2):
        #if len(lin1) != len(lin2):
        #    print "number of events do not match"
        #    return None
        new_gr = gr.copy()
        for i in range(min(len(lin1), len(lin2))):
            ev1 = lin1[i]
            ev2 = lin2[i]
            new_gr.add_edge(ev2, ev1)
        new_gr = transitive_closure(new_gr)
        gps.append(new_gr)
    return gps


def join_graphs(gps, label):
    gps1 = []
    for gr in gps:
        new_graphs = join_graph(gr, label)
        if new_graphs is None:
            return None
        gps1 += new_graphs
    return gps1


def language(gr):
    return [[gr.node[n]["label"] for n in s2] for s2 in linearizations(gr)]


def get_all_prefixes(gr):
    nodes = set(gr.nodes())
    to_process = set()
    to_process.add(frozenset())
    prefixes = []

    while (len(to_process) > 0):
        prefix = to_process.pop()
        for n in nodes.difference(prefix):
            deps = set([a for (a, b) in gr.in_edges(n)])
            if len(deps.difference(prefix)) > 0:
                continue
            to_process.add(frozenset(prefix.union([n])))
        prefixes.append(prefix)
    return prefixes


def get_all_prefix_graphs(gr):
    prefixes = get_all_prefixes(gr)
    print len(prefixes)
    return [nx.subgraph(gr, pr) for pr in prefixes]

