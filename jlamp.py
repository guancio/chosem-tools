import networkx as nx
from utils import *


def gen_non_det_1(msg):
    gr = nx.DiGraph()
    abx = add_pair(gr, "a", "b", 1, msg)
    return gr

example_1 = [gen_non_det_1("x"), gen_non_det_1("y")]

def gen_non_det_2():
    gr1 = nx.DiGraph()
    abx = add_pair(gr1, "a", "b", 1, "x")
    bcz = add_pair(gr1, "b", "c", 2, "z")
    gr1.add_edge(abx[1], bcz[0])

    gr2 = nx.DiGraph()
    acy = add_pair(gr2, "a", "c", 1, "y")
    cbw = add_pair(gr2, "c", "b", 2, "w")
    gr2.add_edge(acy[1], cbw[0])
    return [gr1, gr2]


example_2 = gen_non_det_2()

def gen_non_det_wrong_1(p):
    gr1 = nx.DiGraph()
    abx = add_pair(gr1, "a", p, 1, "x")
    return gr1


example_3 = [gen_non_det_wrong_1("b"), gen_non_det_wrong_1("c")]

def gen_non_det_wrong_2(complete):
    gr1 = nx.DiGraph()
    abx = add_pair(gr1, "a", "b", 1, "x")
    if not complete:
        return gr1
    aby = add_pair(gr1, "a", "b", 2, "y")
    bcz = add_pair(gr1, "b", "c", 3, "z")
    return gr1


example_4 = [gen_non_det_wrong_2(False), gen_non_det_wrong_2(True)]

def gen_non_det_wrong_3(p1, p2):
    gr1 = nx.DiGraph()
    abx = add_pair(gr1, p1, p2, 1, "x")
    return gr1
example_5 = [gen_non_det_wrong_3("a", "b"), gen_non_det_wrong_3("c", "d")]


def gen_example_6():
    gr1 = nx.DiGraph()
    abx = add_pair(gr1, "a", "b", 1, "x")
    cby = add_pair(gr1, "c", "b", 2, "x")
    abz = add_pair(gr1, "a", "b", 3, "z")
    gr1.add_edge(abx[1], abz[1])
    gr1.add_edge(cby[1], abz[1])
    gr1.add_edge(abx[0], abz[0])

    gr2 = nx.DiGraph()
    abx = add_pair(gr2, "a", "b", 1, "y")
    cby = add_pair(gr2, "c", "b", 2, "y")
    abz = add_pair(gr2, "a", "b", 3, "z")
    gr2.add_edge(abx[1], cby[1])
    gr2.add_edge(cby[1], abz[1])
    gr2.add_edge(abx[0], abz[0])

    return [gr1, gr2]

example_6 = gen_example_6()

def gen_graph_choice_par(a_before_d):
    gr = nx.DiGraph()
    abx = add_pair(gr, "a", "b", 1, "x")
    dby = add_pair(gr, "d", "b", 2, "y")
    acx = add_pair(gr, "a", "c", 3, "x")
    dcy = add_pair(gr, "d", "c", 4, "y")
    if a_before_d:
        gr.add_edge(acx[1], dcy[1])
        gr.add_edge(abx[1], dby[1])
    else:
        gr.add_edge(dcy[1], acx[1])
        gr.add_edge(dby[1], abx[1])
    return gr


example_7 = [gen_graph_choice_par(False), gen_graph_choice_par(True)]


def create_thread_parallel_same_msg(th, msg):
    gr = nx.DiGraph()
    ac1 = add_pair(gr, "a", "c", th*4+1, "%s1" % msg)
    bc2 = add_pair(gr, "b", "c", th*4+2, "%s2" % msg)
    ab3 = add_pair(gr, "a", "b", th*4+3, "x")
    bc4 = add_pair(gr, "b", "c", th*4+4, "%s3" % msg)
    gr.add_edge(ac1[0], ab3[0])
    gr.add_edge(bc2[0], ab3[1])
    gr.add_edge(ab3[1], bc4[0])
    gr.add_edge(ac1[1], bc2[1])
    gr.add_edge(bc2[1], bc4[1])

    return gr


def gen_parallel_same_msg():
    G1 = create_thread_parallel_same_msg(1, "l")
    G2 = create_thread_parallel_same_msg(2, "r")
    G = nx.union(G1, G2)
    G = transitive_closure(G)
    return G

example_8 = [gen_parallel_same_msg()]

def gen_parallel_same_msg_simple():
    gr = nx.DiGraph()
    abx1 = add_pair(gr, "a", "b", 1, "x")
    abx2 = add_pair(gr, "a", "b", 2, "x")
    abx3 = add_pair(gr, "a", "b", 3, "y")
    abx4 = add_pair(gr, "a", "b", 4, "z")
    gr.add_edge(abx1[0], abx3[0])
    gr.add_edge(abx2[0], abx4[0])
    gr.add_edge(abx1[1], abx3[1])
    gr.add_edge(abx2[1], abx4[1])
    return gr

parallel_same_msg_simple = [gen_parallel_same_msg_simple()]


i = 1

def seq(graphs1, graphs2):
    res = []
    for gr1 in graphs1:
        for gr2 in graphs2:
            gr = nx.union(gr1, gr2)
            for p in get_all_principals(gr):
                gr1_p = proj(gr1, p)
                gr2_p = proj(gr2, p)
                for n1 in gr1_p.nodes():
                    for n2 in gr2_p.nodes():
                        gr.add_edge(n1, n2)
            res.append(gr)
    return res

def choice(graphs_list):
    res = []
    for glist in graphs_list:
        res.extend(glist)
    return res

def par(graphs1, graphs2):
    res = []
    for gr1 in graphs1:
        for gr2 in graphs2:
            gr = nx.union(gr1, gr2)
            res.append(gr)
    return res
        

def msg_node(snd, rcv, msg):
    global i
    gr = nx.DiGraph()
    add_pair(gr, snd,rcv, i, msg)
    i+=1
    return [gr]
    
def create_case_study(i):
    a = msg_node("a", "c", "advert")
    b = msg_node("a", "c", "advert")
    c = msg_node("b", "a", "get_balance")
    for x in range(i-1):
        a = seq(a, msg_node("a", "c", "m1" + str(x)))
        b = seq(b, msg_node("a", "c", "m2" + str(x)))
        c = seq(c, msg_node("a", "c", "m3" + str(x)))
    return \
        seq(
            seq(
                msg_node("c", "a", "auth"),
                msg_node("a", "b", "auth_request")
            ),
            choice([
                seq(
                    msg_node("b", "a", "denied"),
                    msg_node("a", "c", "auth_failed")
                ),
                seq(
                    msg_node("b", "a", "granted"),
                    choice([
                        msg_node("c", "a", "quit"),
                        seq(
                            msg_node("c", "a", "check_balance"),
                            seq(
                                par(
                                    a,
                                    par(
                                        b,
                                        c
                                    )
                                ),
                                msg_node("a", "c", "balance")
                            )
                        ),
                        seq(
                            msg_node("c", "a", "withdraw"),
                            seq(
                                msg_node("a", "b", "auth_withdraw"),
                                choice([
                                    seq(
                                        msg_node("b", "a", "allow"),
                                        msg_node("a", "c", "money")
                                    ),
                                    seq(
                                        msg_node("b", "a", "deny"),
                                        msg_node("a", "c", "bye")
                                    )
                                ])
                            )
                        )
                    ])                    
                )
            ])
        )

case_study = [create_case_study(i) for i in range(10)]
