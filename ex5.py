import networkx as nx
from utils import add_pair, debug_graph


def gen_graph1():
    gr = nx.DiGraph()
    ab1 = add_pair(gr, "a", "b", 1, "x")
    ab2 = add_pair(gr, "a", "b", 2, "z")
    cb1 = add_pair(gr, "c", "b", 3, "x")
    
    gr.add_edge(ab1[1], ab2[1])
    gr.add_edge(cb1[1], ab2[1])
    return gr

def gen_graph2():
    gr = nx.DiGraph()
    ab1 = add_pair(gr, "a", "b", 1, "y")
    ab2 = add_pair(gr, "a", "b", 2, "z")
    cb1 = add_pair(gr, "c", "b", 3, "y")
    
    gr.add_edge(ab1[0], ab2[0])
    gr.add_edge(ab1[1], cb1[1])
    gr.add_edge(cb1[1], ab2[1])
    return gr

graphs = [gen_graph1(), gen_graph2()]

if __name__ == "__main__":
    print (len(graphs))
    debug_graph(graphs[0])
