#!/usr/bin/python

import networkx as nx
from utils import debug_graph, proj, join_graphs, transitive_closure
from utils import transitive_reduction
from utils import language, get_all_receive_lbls, get_all_prefixes, get_all_prefix_graphs
from utils import get_all_send_lbls, get_matching_label, proj_lbl
import itertools
import shutil
import os

import networkx.algorithms.isomorphism as iso
from networkx.algorithms import isomorphism

import time

nm = iso.categorical_node_match('label', '')


def get_principal_threads(graphs, principals):
    local_threads = {}
    for p in principals:
        gps = []
        i = 0
        for g in graphs:
            prog_gr = proj(g, p)
            debug_graph(prog_gr, "thread-%s-%d" % (p, i))
            toAdd = True
            for other_g in gps:
                if (nx.is_isomorphic(prog_gr, other_g, node_match=nm)):
                    toAdd = False
                    break
            if toAdd:
                gps.append(prog_gr)
            i += 1
        local_threads[p] = gps
    return local_threads


def get_prefixes(graphs):
    res_prefixes = []
    for g in graphs:
        prefixes = get_all_prefix_graphs(g)
        for pr in prefixes:
            toAdd = True
            for other_g in res_prefixes:
                if (nx.is_isomorphic(pr, other_g, node_match=nm)):
                    toAdd = False
                    break
            if toAdd:
                res_prefixes.append(pr)
    return res_prefixes


def make_typles(local_threads):
    principals = local_threads.keys()
    tuples = []
    elems = [[(p, t) for t in local_threads[p]] for p in principals]
    gr_tuples = list(itertools.product(*elems))
    for gr in gr_tuples:
        tpl = {}
        for e in gr:
            tpl[e[0]] = e[1]
        tuples.append(tpl)
    return tuples


def inter_process_closure(tuples, complete):
    ipc = []
    for tpl in tuples:
        principals = tpl.keys()
        gr = tpl[principals[0]]
        for p in principals[1:]:
            gr = nx.union(gr, tpl[p])
        grs = [gr]
        # important this is done on receive,
        # or we will not remove unmatched events
        inputs = get_all_receive_lbls(gr)
        outputs = get_all_send_lbls(gr)
        is_complete = True
        not_matched_out = []
        for o in outputs:
            if len(proj_lbl(gr, o).nodes()) != len(proj_lbl(gr, get_matching_label(o)).nodes()):
                is_complete = False
                not_matched_out.append(o)
        if not is_complete:
            if complete:
                continue
            #else:
            #    print "Not complete", not_matched_out

        for l in inputs:
            grs = join_graphs(grs, l)
            if grs is None:
                break
        if grs is not None:
            ipc = ipc + grs
    return ipc


def cc2pom(ipc, graphs):
    matches = {}
    for i in range(len(ipc)):
        g1 = ipc[i]
        for j in range(len(graphs)):
            g2 = graphs[j]
            g3 = transitive_closure(g2)
            m = isomorphism.GraphMatcher(g1, g3, nm)
            # actually should not be a subgraph
            if m.subgraph_is_isomorphic():
                matches[i] = j
    return matches

def cc3pom(ipc, graphs):
    matches = {}
    for i in range(len(ipc)):
        g1 = ipc[i]
        for j in range(len(graphs)):
            if len(graphs[j].nodes()) != len(ipc[i].nodes()):
                continue
            g2 = graphs[j]
            g4 = transitive_closure(g2)
            #g4 = g2
            m = isomorphism.GraphMatcher(g1, g4, nm)
            # actually should not be a subgraph
            if m.subgraph_is_isomorphic():
                matches[i] = j
                break
    return matches


def tuple_union(tpl):
    principals = tpl.keys()
    gr = tpl[principals[0]]
    for p in principals[1:]:
        gr = nx.union(gr, tpl[p])
    return gr

# for CC2'
def graph_from_word(word):
    gr = nx.DiGraph()
    count = {}
    node = None
    for e in word:
        n = count.get(e, 0)+1
        count[e] = n
        name = e.lower() if e[2] == "!" else (e[1]+e[0]+e[2:]).lower()
        node1 = ("%s-%d" % (name, n))
        gr.add_node(node1)
        gr.node[node1]['label'] = e
        if node is not None:
            gr.add_edge(node, node1)
        node = node1
    return gr

def fast_merge(gr):
    nodes = gr.nodes()
    for node in nodes:
        if node[2] != "!":
            continue
        other_node = (node[1]+node[0]+"?"+node[3:])
        gr.add_edge(node, other_node)

def msc(graphs, principals):
    mscs = []
    for gr in graphs:
        res = []
        for pr in principals:
            grs = []
            grA = proj(gr, pr)
            lang = language(grA)
            for w in lang:
                grs.append(graph_from_word(w))
            res.append(grs)
        tpls = list(itertools.product(*res))
        for tpl in tpls:
            gr = tpl[0]
            for gr1 in tpl[1:]:
                gr = nx.union(gr, gr1)
            fast_merge(gr)
            gr = transitive_closure(gr)
            mscs.append(gr)
    return mscs

def build_T(mscs, principals):
    mscs2 = []
    for m in mscs:
        m1 = []
        for p in range(len(principals)):
            grA = proj(m, principals[p])
            lang = language(grA)
            m1.append(lang[0])
        mscs2.append(m1)
    T = {}
    for s in range(len(mscs2)):
        T[s] = {}
        for t in range(len(mscs2)):
            T[s][t] = {}
            for i in range(len(principals)):
                mt = mscs2[t][i]
                ms = mscs2[s][i]
                T[s][t][i] = -1
                for x in range(min(len(mt), len(ms))):
                    if mt[x] != ms[x]:
                        T[s][t][i] = x
                        break
    U = {}
    for s in range(len(mscs2)):
        ms = mscs2[s]
        U[s] = {}
        for p in range(len(principals)):
            U[s][p] = {}
            for x in range(len(ms[p])-1,-1,-1):
                U[s][p][x] = {}
                if ms[p][x].find("?") >= 0:
                    if not(U[s][p].has_key(x+1)):
                        for j in range(len(principals)):
                            U[s][p][x][j] = len(ms[j])+1
                    else:
                        for j in range(len(principals)):
                            U[s][p][x][j] = U[s][p][x+1][j]
                else:
                    for j in range(len(principals)):
                            U[s][p][x][j] = 0
    # for s in range(len(mscs2)):
    #     for p1 in range(len(principals)):
    #         for x in mscs2[s][p1]:
    #             for p in range(len(principals)):
    #                 for y in mscs2[s][p1]:
    #                     a = 2

    # for s in range(len(mscs2)):
    #     for t in range(len(mscs2)):
    #         for j in range(len(principals)):
    #             for p in range(len(mscs2)):
    # #                 for j1 in range(len(principals)):
    #                     a = 3
                    
        
                    
# MS = {}
# for p in ["a", "b", "c"]:
#     ms = []
#     for m in mscs:
#         v = proj(m, p)
#         v = language(v)[0]
#         ms.append(v)
#     MS[p] = ms


# def prefix(w):
#     r = []
#     for x in range(1, len(w)+1):
#         r.append(w[0:x])
#     return r


# V = {}
# W = {}
# for m in mscs:
#     V[m] = {}
#     W[m] = {}
#     for p in ["a", "b", "c"]:
#         v = proj(m, p)
#         v = language(v)[0]
#         res = []
#         for w in prefix(v):
#             if MS[p].count(w) > 0:
#                 res.append(w)
#         V[m][p] = res
#         # V should be ordered by prefix
#         # there is no need of building segments, since in the example there is only one
#         if len(res) > 1:
#             print res
#         W[m][p] = res




# From here eexperiments for CCc-conditions

# words = []
# for w in language(ex1.graphs[0]):
#     words.append(w)
# print len(words)


# graphs = []
# for pr in ["a", "b", "c"]:
#     grs = []
#     for gr in ex3.graphs:
#         gr = proj(gr, pr)
#         lang = language(gr)
#         grs += lang
#     graphs.append(grs)

# mscs = list(itertools.product(*graphs))


# print len(mscs)







# for s in range(len(mcsc)):
#     for p in range(len(["a", "b", "c"])):
#         for x in mscs[s][p]:
#             pass
# print "Done T"

# # work for CC2'















def run_test(graphs, path, principals, dbg_graphs):
    res = {}
    try:
        shutil.rmtree(path)
    except:
        pass
    os.makedirs(path)
    if dbg_graphs:
        for i in range(len(graphs)):
            debug_graph(transitive_reduction(graphs[i]), "%s/graph-%d" % (path, i))
    local_threads = get_principal_threads(graphs, principals)
    if dbg_graphs:
        for p in principals:
            for i in range(len(local_threads[p])):
                debug_graph(local_threads[p][i], "%s/thread-%s-%d" % (path, p, i))
    start_time = time.time()
    branches = 0
    for p in principals:
        branches += len(local_threads[p])
    print "Number of branches %d" % branches
    res["cc2-branches"] = branches / len(principals)
    tuples = make_typles(local_threads)
    if dbg_graphs:
        for i in range(len(tuples)):
            gr = tuple_union(tuples[i])
            debug_graph(gr, "%s/tuple-%d" % (path, i))
    print "Number of tuples %d" % len(tuples)
    res["cc2-tuples"] = len(tuples)
    ipc = inter_process_closure(tuples, True)
    if dbg_graphs:
        for i in range(len(ipc)):
            debug_graph(transitive_reduction(ipc[i]), "%s/ipc-%d" % (path, i))
    print "Number of ipc %d" % len(ipc)
    res["cc2-ipc"] = len(ipc)
    matches = cc2pom(ipc, graphs)
    f = open("%s/matches" % path, "w")
    errors = 0
    for i in range(len(ipc)):
        if (matches.has_key(i)):
            f.write("%d -> %d\n" % (i, matches[i]))
        else:
            f.write("%d error\n" % i)
            errors += 1
    print "CC2-POM errors %d" % errors
    res["cc2-errors"] = errors
    f.close()
    elapsed_time = time.time() - start_time
    print "CC2-POM time %d" % (elapsed_time * 1000)
    res["cc2-time"] = elapsed_time * 1000

    mscs = msc(graphs, principals)
    start_time = time.time()
    T = build_T(mscs, principals)
    elapsed_time = time.time() - start_time
    print "Number of MSCS %d" % len(mscs)
    print "Time for computing T %d" % (elapsed_time * 1000)
    res["mcs"] = len(mscs)
    res["mcs-time"] = elapsed_time * 1000

    start_time = time.time()
    local_prefixes = {}
    branches = 0
    for p in principals:
        prefixes = get_prefixes(local_threads[p])
        print "Number of prefixes of %s %d" % (p, len(prefixes))
        if dbg_graphs:
            for i in range(len(prefixes)):
                debug_graph(prefixes[i], "%s/prefix-%s-%d" % (path, p, i))
        branches += len(prefixes)
        local_prefixes[p] = prefixes
    res["cc3-branches"] = branches / len(principals)
    tuples = make_typles(local_prefixes)
    # for i in range(len(tuples)):
    #     gr = tuple_union(tuples[i])
    #     debug_graph(gr, "%s/prefix-tuple-%d" % (path, i))
    print len(tuples)
    res["cc3-tuples"] = len(tuples)
    ipc = inter_process_closure(tuples, False)
    print "IPC %s"%len(ipc)
    res["cc3-ipc"] = len(ipc)
    if dbg_graphs:
        for i in range(len(ipc)):
            debug_graph(ipc[i], "%s/prefix-ipc-%d" % (path, i))
    prefixes = get_prefixes(graphs)
    print len(prefixes)
    if dbg_graphs:
        for i in range(len(prefixes)):
            debug_graph(prefixes[i], "%s/prefix-%d" % (path, i))
    matches = cc3pom(ipc, prefixes)
    f = open("%s/prefix-matches" % path, "w")
    errors = 0
    for i in range(len(ipc)):
        if (matches.has_key(i)):
            f.write("%d -> %d\n" % (i, matches[i]))
        else:
            f.write("%d error\n" % i)
            errors += 1
    print "errors CC3-POM %d" % errors
    res["cc3-errors"] = errors
    f.close()
    elapsed_time = time.time() - start_time
    print "Time CC3-POM %d" % (elapsed_time*1000)
    res["cc3-time"] = elapsed_time * 1000

    return res
