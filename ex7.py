import networkx as nx
from utils import *
import itertools

i = 1

def seq(graphs1, graphs2):
    res = []
    for gr1 in graphs1:
        for gr2 in graphs2:
            gr = nx.union(gr1, gr2)
            for p in get_all_principals(gr):
                gr1_p = proj(gr1, p)
                gr2_p = proj(gr2, p)
                for n1 in gr1_p.nodes():
                    for n2 in gr2_p.nodes():
                        gr.add_edge(n1, n2)
            res.append(gr)
    return res

def choice(graphs_list):
    res = []
    for glist in graphs_list:
        res.extend(glist)
    return res
    

def msg_node(snd, rcv, msg):
    global i
    gr = nx.DiGraph()
    add_pair(gr, snd,rcv, i, msg)
    i+=1
    return [gr]
    

def create_process():
    return \
        seq(
            seq(msg_node("c", "a", "auth"),
                msg_node("a", "b", "authReq")),
            choice([
                seq(msg_node("b", "a", "granted"),
                    choice([
                        seq(
                            seq(
                                msg_node("c", "a", "withdraw"),
                                msg_node("a", "b", "authWithdraw")
                            ),
                            choice([
                                seq(
                                    msg_node("b", "a", "deny"),
                                    msg_node("a", "c", "bye")
                                ),
                                seq(
                                    msg_node("b", "a", "allow"),
                                    msg_node("a", "c", "money")
                                )
                            ])
                        ),
                        seq(
                            seq(
                                msg_node("c", "a", "checkBalance"),
                                msg_node("a", "b", "getBalance"),
                            ),
                            msg_node("a", "c", "balance")
                        ),
                        msg_node("c", "a", "quit")
                    ])
                ),
                seq(msg_node("b", "a", "denied"),
                    msg_node("a", "c", "authFailed")
                )
            ])
        )


graphs = create_process()
