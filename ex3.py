import networkx as nx
from utils import add_pair, transitive_closure


def create_thread(th, msg):
    gr = nx.DiGraph()
    ac1 = add_pair(gr, "a", "c", th*4+1, "%s1" % msg)
    bc2 = add_pair(gr, "b", "c", th*4+2, "%s2" % msg)
    ab3 = add_pair(gr, "a", "b", th*4+3, "x")
    bc4 = add_pair(gr, "b", "c", th*4+4, "%s3" % msg)
    gr.add_edge(ac1[0], ab3[0])
    gr.add_edge(bc2[0], ab3[1])
    gr.add_edge(ab3[1], bc4[0])
    gr.add_edge(ac1[1], bc2[1])
    gr.add_edge(bc2[1], bc4[1])

    return gr


def example1():
    G1 = create_thread(1, "l")
    G2 = create_thread(2, "r")
    G = nx.union(G1, G2)
    G = transitive_closure(G)
    return G

graphs = [example1()]
